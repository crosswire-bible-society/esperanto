\id 2PE
\h
\toc1
\toc2
\toc3
\c 1
\p
\v 1 Simon Petro, servisto kaj apostolo de Jesuo Kristo, al tiuj, kiuj ricevis egale altvaloran fidon, kiel ni, en la justeco de nia Dio kaj Savanto Jesuo Kristo:
\v 2 Graco al vi kaj paco pligrandiĝu en sciado pri Dio, kaj pri Jesuo, nia Sinjoro;
\v 3 pro tio, ke lia Dia povo donacis al ni ĉion rilatantan al vivo kaj pieco, per la scio de tiu, kiu nin vokis per sia propra gloro kaj virto;
\v 4 per kiuj li donacis al ni la promesojn karegajn kaj tre grandajn; por ke per ili vi fariĝu partoprenantoj en Dia naturo, forsaviĝinte el la putreco,
\q kiu estas en la mondo per voluptemo.
\v 5 Kaj pro tiu sama kaŭzo viaflanke aplikante ĉian diligentecon, en via fido aldonu virton; kaj en virto scion;
\v 6 kaj en scio sinregadon; kaj en sinregado paciencon; kaj en pacienco piecon;
\v 7 kaj en pieco fratamikecon, kaj en fratamikeco amon.
\v 8 Ĉar tiuj ecoj, se ĉe vi ili troviĝas kaj abundas, faras vin nek mallaboremaj nek senfruktaj en la scio de nia Sinjoro Jesuo Kristo.
\v 9 Ĉar tiu, al kiu mankas tiuj ecoj, estas blinda, miopa, forgesinte la forpurigon el siaj antaŭaj pekoj.
\v 10 Sekve, fratoj, pli diligente klopodu, por certigi vian vokon kaj elekton; ĉar farante tion, vi neniam falpuŝiĝos;
\v 11 ĉar tiel al vi estos riĉe provizita la eniro en la eternan regnon de nia Sinjoro kaj Savanto Jesuo Kristo.
\v 12 Pro tio mi ĉiam estos preta rememorigi vin pri tiuj aferoj, kvankam vi ilin scias kaj estas fortigitaj en la ĉeestanta vero.
\v 13 Kaj mi opinias konvena, dum mi estas en ĉi tiu tabernaklo, vigligi vin per rememorigo;
\v 14 sciante, ke baldaŭ estos la demeto de mia tabernaklo, ĝuste kiel nia Sinjoro Jesuo Kristo montris al mi.
\v 15 Kaj mi diligente penos, ke post mia foriro vi povu konstante rememori tion.
\v 16 Ĉar ne sekvinte fabelojn sofismajn, ni konigis al vi la potencon kaj alvenon de nia Sinjoro Jesuo Kristo, sed vidinte per propraj okuloj lian majeston.
\v 17 Ĉar li ricevis de Dio, la Patro, honoron kaj gloron, kiam al li tia voĉo estis alportita el la brilega gloro: Ĉi tiu estas Mia Filo, la amata,
\q en kiu Mi havas plezuron;
\v 18 kaj tiun voĉon, alportitan el la ĉielo, ni mem aŭdis, kunestante kun li sur la sankta monto.
\v 19 Kaj ni havas la profetan vorton konfirmitan; kiun atentante, vi bone faras, kvazaŭ lampon lumantan en malhela loko, ĝis ektagiĝos kaj la matenstelo ekleviĝos en viaj koroj;
\v 20 antaŭ ĉio sciante, ke neniu profetaĵo de la Skribo enhavas en si sian propran klarigon.
\v 21 Ĉar neniam profetaĵo estas alportita per homa volo; sed homoj, movataj de la Sankta Spirito, parolis laŭ Dio.
\c 2
\p
\v 1 Sed ankaŭ falsaj profetoj troviĝis inter la popolo tiel same, kiel inter vi estos falsaj instruistoj, kiuj sekrete enkondukos herezojn pereigajn, eĉ malkonfesante la Sinjoron, kiu ilin elaĉetis, kaj venigante sur sin rapidan pereon.
\v 2 Kaj ilian senbridecon sekvos multaj, per kiuj la vojo de la vero estos kalumniata.
\v 3 Kaj en avideco per trompaj paroloj ili vin komercos; ilia kondamno nun de post tre longe ne malfruas, kaj ilia pereo ne dormas.
\v 4 Ĉar se Dio ne indulgis anĝelojn pekintajn, sed, eninferiginte ilin en kavernojn de mallumo, transdonis ilin rezervatajn por la juĝo;
\v 5 kaj ne indulgis la mondon antikvan, sed gardis Noan kun sep aliaj,
\q predikanton de justeco, kiam Li sendis diluvon sur la mondon de malpiuloj;
\v 6 kaj, cindriginte la urbojn Sodom kaj Gomora, kondamnis ilin per katastrofo, farinte ilin ekzemplo al estontaj malpiuloj;
\v 7 kaj savis justan Loton, ĉagrenegatan de la senbrida konduto de la malvirtuloj
\v 8 (ĉar tiu justulo, loĝante inter ili, en vidado kaj aŭdado, turmentis sian justan animon, tagon post tago, pro iliaj malvirtaj faroj):
\v 9 la Sinjoro do scias savi el tento la piulojn, kaj teni sub puno la maljustulojn ĝis la tago de juĝo;
\v 10 sed precipe tiujn, kiuj iras laŭ la karno en volupto de malpuraĵo, kaj malestimas regadon. Arogantaj, obstinaj, ili ne timas insulti aŭtoritatojn;
\v 11 sed kontraŭe, anĝeloj, superante laŭ forto kaj potenco, ne faras insultan akuzon kontraŭ ili antaŭ la Sinjoro.
\v 12 Sed tiuj, kiel bestoj senprudentaj, naskitaj laŭ naturo por kaptado kaj pereo, blasfemante en aferoj, kiujn ili ne scias, en sia putreco pereos,
\v 13 en malbono ricevante pagon por malbono; homoj, kiuj opinias entagan diboĉon plezuro kaj estas makuloj kaj hontindaĵoj, diboĉante en sia uzado de la agapoj, dum ili kunfestenas kun vi;
\v 14 havante okulojn plenajn de adulto kaj ne reteneblajn de pekado;
\q forlogante malfirmajn animojn; havante koron lertan por avideco; estante filoj de malbeno;
\v 15 forlasinte la rektan vojon kaj erarvaginte, sekvinte la vojon de Bileam,
\q filo de Beor, kiu amis la rekompencon de malbonfarado;
\v 16 sed li estis riproĉita pro sia malobeo; muta azeno, parolante per homa voĉo, haltigis la frenezecon de la profeto.
\v 17 Ili estas putoj senakvaj, kaj nebuletoj pelataj de ventego; por ili la nigreco de mallumo estas rezervita.
\v 18 Ĉar, elparolante fanfaronaĵojn de vanteco, ili forlogas en la karnovolupton per senbrideco tiujn, kiuj ĵus forsaviĝis de tiuj, kiuj vivadas en eraro;
\v 19 anoncante al ili liberecon, ili mem estas sklavoj de putreco; ĉar al kiu iu submetiĝas, al tiu ankaŭ li sklaviĝas.
\v 20 Ĉar se, forsaviĝinte el la malpuraĵoj de la mondo per la scio de la Sinjoro kaj Savanto Jesuo Kristo, ili estos denove tien implikitaj kaj venkitaj, ilia lasta stato fariĝos pli malbona, ol la unua.
\v 21 Ĉar estus por ili pli bone ne ekkoni la vojon de justeco, ol, ekkoninte ĝin, returni sin for de la sankta ordono al ili transdonita.
\v 22 Al ili okazis laŭ la vera proverbo: Hundo reveninta al sia vomitaĵo,
\q kaj porkino lavita al ruliĝado en koto.
\c 3
\p
\v 1 Jam ĉi tiun duan epistolon, amataj, mi skribas al vi; en ambaŭ mi instigas vian sinceran menson per rememorigo;
\v 2 por ke vi memoru la antaŭdirojn de la sanktaj profetoj, kaj la ordonon de la Sinjoro kaj Savanto per viaj apostoloj;
\v 3 unue sciante, ke en la lastaj tagoj mokemuloj venos kun mokado, irante laŭ siaj propraj voluptoj,
\v 4 kaj dirante: Kie estas la anonco de lia alveno? ĉar de kiam la patroj endormiĝis, ĉio restas kiel de post la komenco de la kreo.
\v 5 Ĉar ili volonte forgesas, ke ĉielo ekzistis de antikve, kaj tero kunmetita el akvo kaj meze de akvo, laŭ la vorto de Dio;
\v 6 per kio la tiama mondo, diluvite, pereis;
\v 7 sed la nuna ĉielo kaj la tero per la sama vorto estas destinitaj por fajro, rezervate ĝis la tago de juĝo kaj pereo de malpiuloj.
\v 8 Sed ne forgesu ĉi tiun unu aferon, amataj, ke ĉe la Sinjoro unu tago estas kiel mil jaroj, kaj mil jaroj kiel unu tago.
\v 9 La Sinjoro ne malrapidas pri la promeso, kiel iuj malrapidecon kalkulas;
\q sed paciencas al vi, volante, ne ke iuj pereu, sed ke ĉiuj venu al pento.
\v 10 Sed la tago de la Sinjoro venos, kvazaŭ ŝtelisto; en tiu tago la ĉielo forpasos kun muĝa bruego, kaj la elementoj brulante solviĝos, kaj la tero kaj la faritaĵoj en ĝi forbrulos.
\v 11 Ĉar tiamaniere ĉio tio solviĝos, kiaj homoj vi do devus esti en sankta konduto kaj pieco,
\v 12 atendante kaj akcelante la alvenon de la tago de Dio, pro kio la ĉielo flamanta solviĝos, kaj la elementoj per fajra brulado fluidiĝos?
\v 13 Sed laŭ Lia promeso ni atendas novan ĉielon kaj novan teron, en kiuj loĝas justeco.
\v 14 Tial, amataj, tion atendante, klopodu troviĝi en paco, senmakulaj kaj neriproĉindaj antaŭ Li.
\v 15 Kaj rigardu la paciencon de nia Sinjoro kiel savon; kiel ankaŭ nia amata frato Paŭlo, laŭ la saĝeco donita al li, jam skribis al vi,
\v 16 kiel ankaŭ en ĉiuj siaj epistoloj, parolante en ili pri ĉi tio; en kiuj estas iuj aferoj malfacile kompreneblaj, kiujn la malkleruloj kaj malkonstantuloj tordas, kiel ankaŭ la ceterajn skribaĵojn, al sia propra pereo.
\v 17 Vi do, amataj, ĉi tion antaŭsciante, gardu vin, por ke vi ne forlogiĝu per la eraro de la pekuloj, kaj ne defalu de via propra konstanteco.
\v 18 Sed kresku en graco kaj scio de nia Sinjoro kaj Savanto Jesuo Kristo. Al li estu la gloro nun kaj ĝis la tago de eterneco. Amen.
