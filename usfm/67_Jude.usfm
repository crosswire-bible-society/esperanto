\id JUD
\h
\toc1
\toc2
\toc3
\c 1
\p
\v 1 Judas, servisto de Jesuo Kristo kaj frato de Jakobo, al la alvokitoj,
\q amataj en Dio, la Patro, kaj konservitaj por Jesuo Kristo:
\v 2 Kompato al vi kaj paco kaj amo pligrandiĝu.
\v 3 Amataj, kiam mi faris ĉian diligentecon, por skribi al vi pri nia komuna savo, mi deviĝis skribi al vi, por kuraĝigi vin batali por la kredo, jam per unu fojo transdonita al la sanktuloj.
\v 4 Ĉar enŝteliĝis iuj homoj, jam antaŭ longe destinitaj por ĉi tiu kondamno, malpiuloj, ŝanĝante la gracon de nia Dio en diboĉecon, kaj malkonfesante la solan Estron kaj nian Sinjoron Jesuo Kristo.
\v 5 Mi do volas rememorigi vin, kvankam vi jam per unu fojo sciiĝis pri ĉio,
\q ke la Sinjoro, savinte popolon el la Egipta lando, poste pereigis la nekredantojn.
\v 6 Kaj anĝelojn, kiuj ne konservis sian regadon, sed forlasis sian propran loĝejon, Li rezervis sub mallumo en katenoj ĉiamaj ĝis la juĝo en la granda tago.
\v 7 Kiel ankaŭ Sodom kaj Gomora kaj la ĉirkaŭaj urboj tiel same, kiel ĉi tiuj, malĉastiĝinte kaj foririnte post fremdan karnon, estas elmontritaj kiel ekzemplo, suferante la punon de eterna fajro.
\v 8 Tamen tiel same ankaŭ ĉi tiuj en siaj sonĝadoj malpurigas la karnon,
\q malestimas aŭtoritaton, kaj insultas honorojn.
\v 9 Sed Miĥael, la ĉefanĝelo, kiam en kontraŭstaro al la diablo li disputis pri la korpo de Moseo, ne kuraĝis lin akuzi insulte, sed diris: La Sinjoro vin riproĉu.
\v 10 Sed tiuj insultas ja ĉion, kion ili ne scias; sed kion ili per naturo komprenas, kiel la bestoj senprudentaj, en tio ili malvirtiĝas.
\v 11 Ve al ili! ĉar ili iris sur la vojo de Kain, kaj forĵetis sin en la eraron de Bileam por dungopago, kaj pereis en la kontraŭdirado de Koraĥ.
\v 12 Ili estas la subakvaj rokoj en viaj agapoj, kun vi kunfestenante, sentime sin paŝtante; nuboj senakvaj, per vento disportataj; aŭtunaj arboj senfruktaj, dufoje mortintaj, elradikigitaj;
\v 13 sovaĝaj marondoj, elŝaŭmantaj siajn hontindaĵojn; steloj vagantaj, por kiuj la nigreco de mallumo por eterne estas rezervata.
\v 14 Kaj al ili ankaŭ Ĥanoĥ, la sepa post Adam, profetis, dirante: Jen la Sinjoro venis kun Siaj sanktaj miriadoj,
\v 15 por fari juĝon kontraŭ ĉiuj, kaj por kondamni ĉiujn malpiulojn pri ĉiuj malpiaĵoj, kiujn ili malpie faris, kaj pri ĉiuj obstinaj paroloj,
\q kiujn malpiaj pekuloj parolis kontraŭ Li.
\v 16 Ili estas murmuremuloj, plendemuloj, irantaj laŭ siaj voluptoj (dum ilia buŝo parolas fanfaronaĵojn) kaj favorantaj personojn pro profito.
\v 17 Sed vi, amataj, memoru la dirojn antaŭe parolitajn de la apostoloj de nia Sinjoro Jesuo Kristo;
\v 18 nome, ke ili diris al vi: En la lasta tempo estos mokemuloj, irantaj laŭ siaj propraj voluptoj malpiaj.
\v 19 Tiuj estas la apartigantoj, laŭsentaj, ne havantaj la Spiriton.
\v 20 Sed vi, amataj, konstruante vin sur via plej sankta fido, preĝante en la Sankta Spirito,
\v 21 konservu vin en la amo al Dio, atendante la kompaton de nia Sinjoro Jesuo Kristo por eterna vivo.
\v 22 Kaj unujn, kiuj ŝanceliĝas, indulgu;
\v 23 kaj unujn savu, eltirante ilin el la fajro; kaj aliajn kompatu kun timo;
\q malamante eĉ la veston makulitan de la karno.
\v 24 Kaj al Tiu, kiu povas vin gardi senfalaj, kaj starigi vin senriproĉaj antaŭ Sia gloro en granda ĝojo,
\v 25 al la sola Dio, nia Savanto, per Jesuo Kristo, nia Sinjoro, estu gloro,
\q majesto, potenco, kaj aŭtoritato, antaŭ ĉiu mondaĝo, kaj nun, kaj ĝis la eterneco. Amen.
