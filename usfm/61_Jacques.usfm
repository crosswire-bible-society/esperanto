\id JAS
\h
\toc1
\toc2
\toc3
\c 1
\p
\v 1 Jakobo, servisto de Dio kaj de la Sinjoro Jesuo Kristo, al la dek du triboj, kiuj estas dispelitaj: Saluton!
\v 2 Rigardu ĉion kiel ĝojigan, miaj fratoj, kiam vi falas en diversajn tentojn,
\v 3 sciante, ke la provado de via fido faras paciencon.
\v 4 Kaj la pacienco havu sian perfektan faradon, por ke vi estu perfektaj kaj kompletaj, ne havante mankon.
\v 5 Sed se al iu el vi mankas saĝeco, li petu Dion, kiu donacas al ĉiuj malavare kaj ne riproĉas, kaj ĝi estos al li donata.
\v 6 Sed li petu en fido, neniel dubante; ĉar la dubanto similas ondon de la maro, ventopelatan kaj skuatan.
\v 7 Ĉar tiu homo ne pensu, ke li ricevos ion de la Sinjoro-
\v 8 duoblanima homo, ŝanceliĝa en ĉiuj siaj vojoj.
\v 9 Sed la malaltranga frato ĝoju pri sia alteco;
\v 10 kaj la riĉulo pri sia humiliĝo; ĉar kiel floro de herbo li forpasos.
\v 11 Ĉar la suno leviĝas kun la brulvento kaj sekigas la herbon, kaj ĝia floro falas, kaj la gracio de ĝia formo pereas; tiel ankaŭ la riĉulo velkos en siaj vojoj.
\v 12 Feliĉa estas la homo, kiu elportas tenton; ĉar kiam li estos elprovita, li ricevos la kronon de vivo, kiun la Sinjoro promesis al tiuj,
\q kiuj lin amas.
\v 13 Neniu diru, kiam li estas tentata: Mi estas tentata de Dio; ĉar Dio ne estas tentebla de malbono, kaj Li mem tentas neniun;
\v 14 sed ĉiu estas tentata, kiam li estas fortirata de sia dezirado, kaj delogata.
\v 15 Tiam la dezirado, gravediĝinte, naskas pekon; kaj la peko,
\q maturiĝinte, naskas morton.
\v 16 Ne trompiĝu, miaj amataj fratoj.
\v 17 Ĉiu bona donaĵo kaj ĉiu perfekta donaco estas de supre,
\q malsuprenvenante de la Patro de lumoj, ĉe kiu ne povas ekzisti ŝanĝo, nek ombro de sinturnado.
\v 18 Laŭ Sia propra volo Li naskis nin per la vorto de la vero, por ke ni estu kvazaŭ unuaaĵo de Liaj kreitaĵoj.
\v 19 Tion vi scias, miaj amataj fratoj. Sed ĉiu rapidu aŭdi, malrapidu paroli, malrapidu koleri;
\v 20 ĉar la kolero de homo ne efektivigas la justecon de Dio.
\v 21 Tial, formetinte ĉian malpurecon kaj superfluon de malico, akceptu kun humileco la enplantitan vorton, kiu povas savi viajn animojn.
\v 22 Sed estu plenumantoj de la vorto, kaj ne nur aŭskultantoj, trompantaj vin mem.
\v 23 Ĉar se iu estas aŭskultanto de la vorto, kaj ne plenumanto, li similas iun, kiu rigardas sian naturan vizaĝon en spegulo;
\v 24 ĉar li sin rigardas kaj foriras, kaj tuj forgesas, kia li estis.
\v 25 Sed tiu, kiu fikse rigardas en la perfektan leĝon, la leĝon de libereco, kaj tiel restas, ne estante aŭskultanto, kiu forgesas, sed plenumanto, kiu energias, tiu estos benata en sia faro.
\v 26 Se iu ŝajnas al si religia, ne bridante sian langon, sed trompante sian koron, ties religio estas vanta.
\v 27 Religio pura kaj senmakula antaŭ nia Dio kaj Patro estas jena: viziti orfojn kaj vidvinojn en ilia mizero, kaj sin gardi sen malpuriĝo de la mondo.
\c 2
\p
\v 1 Miaj fratoj, ne kun personfavorado tenu la fidon de nia Sinjoro Jesuo Kristo, la glora Sinjoro.
\v 2 Ĉar se en vian sinagogon envenas viro kun oraj ringoj, en bela vestaro,
\q kaj envenas ankaŭ malriĉulo en malpura vestaro;
\v 3 kaj vi respektas la bele vestitan homon, kaj diras: Sidiĝu ĉi tie en bona loko; kaj diras al la malriĉulo: Staru tie, aŭ sidiĝu sube apud mia piedbenketo;
\v 4 ĉu vi ne diferencigas en vi mem, kaj fariĝas juĝistoj malbone pensantaj?
\v 5 Aŭskultu, miaj amataj fratoj; ĉu Dio ne elektis la malriĉulojn laŭ la mondo, por esti riĉaj rilate al fido, kaj heredantoj de la regno, kiun Li promesis al tiuj, kiuj Lin amas?
\v 6 Sed vi malhonoradis la malriĉulon. Ĉu ne subpremas vin la riĉuloj, kaj mem trenas vin antaŭ la tribunalojn?
\v 7 Ĉu ili ne blasfemas la honorindan nomon, sur vin metitan?
\v 8 Tamen, se vi plenumas la reĝan leĝon laŭ la Skribo: Amu vian proksimulon kiel vin mem-vi faras bone;
\v 9 sed se vi favoras personojn, vi faras pekon, kulpigitaj de la leĝo, kiel malobeintoj.
\v 10 Ĉar ĉiu, kiu observas la tutan leĝaron, sed falpuŝiĝas pri unu punkto, fariĝis kulpa pri ĉio.
\v 11 Ĉar Tiu, kiu diris: Ne adultu-diris ankaŭ: Ne mortigu. Se do vi ne adultas, sed mortigas, vi fariĝis malobeinto kontraŭ la leĝo.
\v 12 Tiel parolu, kaj tiel agu, kiel juĝotoj laŭ leĝo de libereco.
\v 13 Ĉar al tiu, kiu ne kompatis, la juĝo estos senkompata; la kompato sin altigas super la juĝo.
\v 14 Kia estas la profito, miaj fratoj, se iu diras, ke li havas fidon, sed ne havas farojn? ĉu tiu fido povas lin savi?
\v 15 Se frato aŭ fratino estas nuda kaj sen ĉiutaga manĝo,
\v 16 kaj iu el vi diras al ili: Iru en paco, estu varmaj kaj sataj; tamen vi ne donas al ili tion, kion la korpo bezonas, kia estas la profito?
\v 17 Tiel same fido, ne havante farojn, estas en si mem malviva.
\v 18 Kaj cetere iu diros: Vi havas fidon, kaj mi havas farojn; montru al mi vian fidon aparte de faroj, kaj mi per miaj faroj montros al vi la fidon.
\v 19 Vi ja kredas, ke Dio estas unu; vi faras bone; la demonoj ankaŭ kredas kaj tremas.
\v 20 Se ĉu vi volas scii, ho vantulo, ke la fido sen faroj estas senfrukta?
\v 21 Ĉu nia patro Abraham ne praviĝis per faroj, oferinte sian filon Isaak sur la altaro?
\v 22 Vi vidas, ke la fido kunagis kun liaj faroj, kaj per faroj la fido perfektiĝis;
\v 23 kaj plenumiĝis la Skribo, kiu diras: Kaj Abraham kredis al Dio, kaj tio estis kalkulita al li kiel virto; kaj li estis nomita: amiko de Dio.
\v 24 Vi vidas, ke homo praviĝas per faroj, kaj ne sole per fido.
\v 25 Tiel same, ĉu ankaŭ la malĉastistino Raĥab ne praviĝis per faroj,
\q kiam ŝi akceptis la senditojn kaj forsendis ilin per alia vojo?
\v 26 Ĉar kiel la korpo aparte de la spirito estas malviva, tiel ankaŭ fido aparte de faroj estas malviva.
\c 3
\p
\v 1 Ne estu multaj instruistoj, miaj fratoj, sciante, ke ni ricevos pli severan juĝon.
\v 2 Ĉar multokaze ni ĉiuj falpuŝiĝas. Se iu ne falpuŝiĝas parole, tiu estas perfekta homo, kapabla bridi ankaŭ la tutan korpon.
\v 3 Se ni al la ĉevaloj enmetas la bridojn en la buŝojn, por ke ili obeu al ni, ni ankaŭ ĉirkaŭturnas ilian tutan korpon.
\v 4 Jen ankaŭ la ŝipoj, kiuj, kvankam ili estas tiel grandaj kaj estas kurepelataj de fortaj ventoj, tamen per tre malgranda direktilo turniĝadas,
\q kien ajn la volo de la direktilisto decidas.
\v 5 Tiel ankaŭ la lango estas malgranda membro, kaj fanfaronas grandaĵojn.
\q Jen, kiel grandan arbaron ekbruligas fajrero!
\v 6 Kaj la lango estas fajro; mondo da maljusteco inter niaj membroj estas la lango, kiu malpurigas la tutan korpon kaj ekbruligas la radon de la naturo kaj estas ekbruligita de Gehena.
\v 7 Ĉar ĉiun specon de bestoj kaj birdoj, de rampaĵoj kaj enmaraĵoj la homa raso al si subigas kaj subigis;
\v 8 sed la langon neniu povas subigi; ĝi estas malkvieta malbono, plena de mortiga veneno.
\v 9 Per ĝi ni benas la Sinjoron kaj Patron; kaj per ĝi ni malbenas homojn,
\q faritajn laŭ la bildo de Dio;
\v 10 el la sama buŝo eliras beno kaj malbeno. Miaj fratoj, tio devus ne tiel esti.
\v 11 Ĉu la fonto elŝprucigas el la sama aperturo dolĉan akvon kaj maldolĉan?
\v 12 ĉu figarbo, miaj fratoj, povas doni olivojn, aŭ vinberarbo figojn? kaj sala akvo ne donas dolĉaĵon.
\v 13 Kiu inter vi estas saĝa kaj prudenta? tiu elmontru per honesta vivado siajn farojn en mildeco de saĝeco.
\v 14 Sed se vi havas akran ĵaluzon kaj malpacon en via koro, ne fieru, kaj ne mensogu kontraŭ la vero.
\v 15 Ĉi tiu saĝeco ne devenas de supre, sed estas monda, laŭsenta, demona.
\v 16 Ĉar kie estas ĵaluzo kaj malpaco, tie estas konfuzo kaj ĉia malnobla ago.
\v 17 Sed la saĝeco, kiu estas de supre, estas unue ĉasta, poste pacema,
\q milda, cedema, plena de kompatemo kaj bonaj fruktoj, sen partieco, sen hipokriteco.
\v 18 Kaj la frukto de justeco estas semata en paco por tiuj, kiuj faras pacon.
\c 4
\p
\v 1 De kie militoj kaj de kie bataloj inter vi? ĉu ne de viaj voluptoj,
\q militantaj en viaj membroj?
\v 2 Vi deziras, kaj ne havas; vi mortigas kaj konkuras, kaj ne povas akiri;
\q vi batalas kaj militas; vi ne havas, ĉar vi ne petas.
\v 3 Vi petas kaj ne ricevas, tial, ke vi petas malprave, por ke vi elspezu por viaj voluptoj.
\v 4 Vi adultulinoj, ĉu vi ne scias, ke la amikeco al la mondo estas malamikeco al Dio? Ĉiu do, kiu volas esti amiko de la mondo, fariĝas malamiko de Dio.
\v 5 Aŭ ĉu vi opinias, ke la Skribo vane parolas? Ĉu la spirito, kiun Li loĝigis en ni, deziregas envieme?
\v 6 Sed Li donas pli grandan gracon. Tial estas dirite: Dio kontraŭstaras al la fieruloj, sed al la humiluloj Li donas gracon.
\v 7 Submetiĝu do al Dio; sed rezistu la diablon, kaj li forkuros de vi.
\v 8 Alproksimiĝu al Dio, kaj Li alproksimiĝos al vi. Purigu la manojn, vi pekuloj, kaj ĉastigu la korojn, vi duoblanimuloj.
\v 9 Mizeru kaj malĝoju kaj ploru; via ridado turniĝu en ploron, kaj via ĝojo en malĝojon.
\v 10 Humiliĝu antaŭ la Sinjoro, kaj Li vin altigos.
\v 11 Ne kalumniu unu la alian, fratoj. Kiu kalumnias fraton aŭ juĝas sian fraton, tiu kalumnias la leĝon kaj juĝas la leĝon; sed se vi juĝas la leĝon, vi jam estas ne plenumanto de la leĝo, sed juĝanto.
\v 12 Unu estas la leĝdonisto kaj juĝisto, Tiu, kiu povas savi aŭ pereigi;
\q kiu vi estas, juĝanta vian proksimulon?
\v 13 Atentu nun vi, kiuj diras: Hodiaŭ aŭ morgaŭ ni iros al tiu urbo kaj restos tie unu jaron kaj negocados kaj profitos;
\v 14 kvankam vi ne scias, kio morgaŭ okazos. Kio estas via vivo? Vi ja estas vaporo, kiu mallongan tempon montriĝas kaj poste malaperas.
\v 15 Anstataŭ diri: Se la Sinjoro volos, ni vivados kaj faros ĉi tion aŭ tion.
\v 16 Sed nun vi fieras pri viaj memfidaĵoj; ĉiu tia singratulado estas malbona.
\v 17 Kiu do scias bonfari, kaj ne bonfaras, ĉe tiu estas peko.
\c 5
\p
\v 1 Atentu nun, riĉuloj; ploregu kaj kriegu pro la mizeroj sur vin alvenontaj.
\v 2 Via riĉo putriĝis, kaj viaj vestoj konsumiĝas de tineoj.
\v 3 Via oro kaj via arĝento rustiĝis; kaj ilia rusto atestos kontraŭ vi,
\q kaj konsumos vian karnon, kiel fajro. En la lastaj tagoj vi kolektadis trezoron.
\v 4 Jen krias la de vi trompe retenata salajro de la laboristoj, kiuj falĉis viajn kampojn; kaj la krioj de la rikoltintoj venis en la orelojn de la Eternulo Cebaot.
\v 5 Vi luksadis sur la tero, kaj diboĉadis; vi nutradis viajn korojn en tago de buĉado.
\v 6 Vi kondamnis, vi mortigis la justulon; li ne kontraŭstaras al vi.
\v 7 Paciencu do, fratoj, ĝis la alveno de la Sinjoro. Jen la terkultivisto atendas la multevaloran frukton de la tero, paciencante pri ĝi, ĝis ĝi ricevos la fruan kaj la malfruan pluvon.
\v 8 Vi ankaŭ paciencu; fortikigu viajn korojn; ĉar la alveno de la Sinjoro alproksimiĝas.
\v 9 Ne murmuru, fratoj, unu kontraŭ la alia, por ke vi ne estu juĝataj; jen la juĝisto staras antaŭ la pordoj.
\v 10 Prenu, fratoj, kiel ekzemplon de suferado kaj pacienco, la profetojn,
\q kiuj parolis en la nomo de la Sinjoro.
\v 11 Jen ni nomas feliĉaj tiujn, kiuj elportis suferon; vi aŭdis pri la pacienco de Ijob, kaj vidis la finan agadon de la Sinjoro, ke la Sinjoro estas kompatema kaj indulgema.
\v 12 Sed antaŭ ĉio, miaj fratoj, ne ĵuru, nek per la ĉielo, nek per la tero, nek per ia alia ĵuro; sed via jes estu jes, kaj via ne estu ne; por ke vi ne falu sub juĝon.
\v 13 Ĉu iu el vi suferas? li preĝu. Ĉu iu estas gaja? li psalme kantu.
\v 14 Ĉu iu el vi malsanas? li venigu la presbiterojn de la eklezio; kaj ili preĝu super li, ŝmirinte lin per oleo en la nomo de la Sinjoro;
\v 15 kaj la preĝo de fido savos la malsanulon, kaj la Sinjoro relevos lin;
\q kaj se li faris pekojn, tio estos pardonita al li.
\v 16 Konfesu do viajn pekojn unu al la alia, kaj preĝu unu por la alia, por ke vi resaniĝu. Petego de justulo multe efikas per sia energio.
\v 17 Elija estis homo samnatura, kiel ni, kaj li preĝis fervore, ke ne pluvu; kaj ne pluvis sur la teron dum tri jaroj kaj ses monatoj.
\v 18 Kaj denove li preĝis; kaj la ĉielo donis pluvon, kaj la tero ekproduktis sian frukton.
\v 19 Miaj fratoj, se iu el vi forvagos de la vero, kaj iu lin returnos,
\v 20 oni sciu, ke tiu, kiu returnas pekulon de la elvoja forvago, savos animon el morto kaj kovros amason da pekoj.
