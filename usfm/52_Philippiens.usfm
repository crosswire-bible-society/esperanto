\id PHP
\h
\toc1
\toc2
\toc3
\c 1
\p
\v 1 Paŭlo kaj Timoteo, servistoj de Jesuo Kristo, al ĉiuj sanktuloj en Kristo Jesuo, kiuj estas en Filipi, kun la episkopoj kaj diakonoj:
\v 2 Graco al vi kaj paco estu de Dio, nia Patro, kaj de la Sinjoro Jesuo Kristo.
\v 3 Mi dankas mian Dion ĉe ĉiu rememoro pri vi,
\v 4 ĉiam en ĉiu mia preĝo por vi ĉiuj farante la peton kun ĝojo,
\v 5 pro via partopreno en la evangelio de post la unua tago ĝis nun;
\v 6 pri tio mem fidante, ke Tiu, kiu komencis bonan faron ĉe vi, ĝin perfektigados ĝis la tago de Jesuo Kristo;
\v 7 kaj ja decas, ke mi tion sentu pri vi ĉiuj, ĉar vi havas min en via koro, kaj vi ĉiuj estas kun mi partoprenantoj en la graco, kiel en miaj katenoj, tiel ankaŭ en la defendo kaj fortigado de la evangelio.
\v 8 Ĉar Dio estas mia atestanto, kiel fervore mi sopiras al vi ĉiuj laŭ la koramo de Kristo Jesuo.
\v 9 Kaj mi preĝas, ke via amo abundu ankoraŭ plie kaj plie, en scio kaj ĉia saĝo,
\v 10 por ke vi aprobu la plej bonajn aferojn kaj estu sinceraj kaj senofendaj ĝis la tago de Kristo,
\v 11 plenigite de la fruktoj de justeco, kiuj estas per Jesuo Kristo, al la gloro kaj laŭdo de Dio.
\v 12 Sed, fratoj, mi volas certigi vin, ke miaj aferoj plie efektiviĝis por la progresado de la evangelio,
\v 13 tiel, ke miaj katenoj estas evidentigitaj en Kristo tra la tuta Pretorio kaj ĉie aliloke;
\v 14 kaj la plimulto el la fratoj en la Sinjoro, pro miaj katenoj fariĝinte sentimaj, supermezure kuraĝas persiste elparoladi la vorton de Dio.
\v 15 Unuj ja proklamas Kriston eĉ envie kaj malpace, sed aliaj bonvole;
\v 16 ĉi tiuj kun amo, sciante, ke mi estas metita, por defendi la evangelion;
\v 17 sed tiuj kun partieco predikas Kriston, ne sincere, supozante, ke ili aldonos doloron al miaj katenoj.
\v 18 Kio do? nur tio, ke ĉiamaniere, ĉu pretekste aŭ vere, oni predikas Kriston: kaj pro tio mi ĝojas kaj ankoraŭ plu ĝojos.
\v 19 Ĉar mi scias, ke ĉi tio efikos al mia savo, per viaj preĝoj kaj la provizado de la Spirito de Jesuo Kristo,
\v 20 laŭ mia fervora atendado kaj espero, ke mi pri nenio hontiĝos, sed ke per ĉia liberparolo, kiel ĉiam, tiel ankaŭ nun, Kristo estos glorata en mia korpo, ĉu per vivo aŭ per morto.
\v 21 Ĉar ĉe mi la vivado estas Kristo, kaj morti estas gajno.
\v 22 Sed se mi vivados korpe, tio signifas pluan frukton de laboro; kaj mi ne scias, kion elekti.
\v 23 Ĉar ambaŭflanke mi estas embarasata, havante deziron foriri kaj esti kun Kristo, kio estas multe pli bona;
\v 24 tamen mia restado en la karno estas pli bezona por vi.
\v 25 Kaj tiel fidante, mi scias, ke mi restos kaj apudrestados kun vi ĉiuj,
\q por via progreso kaj ĝojo de fido;
\v 26 por ke mi pli abunde en Kristo Jesuo gratulu min pri vi, per mia denova ĉeestado kun vi.
\v 27 Sed via vivmaniero nur estu inda je la evangelio de Kristo, por ke, ĉu mi alvenos kaj vin vidos, aŭ forestos, mi aŭdu pri viaj aferoj, ke vi fortike staras unuspirite, kunbatalantaj unuanime por la fido de la evangelio,
\v 28 kaj neniel timigitaj de la kontraŭuloj: kio estas al ili certa signo de pereo, sed al vi certa signo (nepre de Dio) pri via saviĝo;
\v 29 ĉar estas al vi permesite pro Kristo, ne sole kredi al li, sed ankaŭ suferi pro li;
\v 30 havantaj la saman bataladon, kiun vi vidis ĉe mi, kaj pri kiu vi nun aŭdas, ke ĉe mi ĝi ankoraŭ ekzistas.
\c 2
\p
\v 1 Se do en Kristo ekzistas ia konsolo, ia simpatio de amo, ia kunuleco de la Spirito, ia korfavoro kaj kompato,
\v 2 plenigu mian ĝojon, ke vi tiel same sentu, havante la saman amon,
\q estante unuanimaj, sampensaj,
\v 3 nenion farante malpace aŭ arogante, sed kun humileco rigardante unu la alian kiel pli indan, ol li mem;
\v 4 ne atentu ĉiu siajn proprajn aferojn, sed ĉiu ankaŭ la aferojn de aliaj.
\v 5 Tiu sama spirito estu en vi, kiu estis ankaŭ en Kristo Jesuo,
\v 6 kiu, estante en la formo de Dio, ne rigardis kiel ŝatindaĵon la egalecon kun Dio
\v 7 sed sin malplenigis, alprenante la formon de sklavo, fariĝante laŭ la bildo de homoj;
\v 8 kaj troviĝinte laŭfigure kiel homo, li sin humiligis kaj fariĝis obeema ĝis morto, eĉ ĝis la morto per kruco.
\v 9 Pro tio do Dio tre alte superigis lin, kaj donis al li nomon, kiu estas super ĉia nomo,
\v 10 por ke en la nomo de Jesuo kliniĝu ĉiu genuo, de enĉieluloj kaj surteruloj kaj subteruloj,
\v 11 kaj ĉiu lango konfesu, ke Jesuo Kristo estas Sinjoro, al la gloro de Dio, la Patro.
\v 12 Tial, miaj amatoj, kiel vi ĉiam obeis, ne nur dum mia ĉeestado ĉe vi,
\q sed nun eĉ pli multe dum mia forestado, ellaboru kun timo kaj tremo vian propran savon;
\v 13 ĉar Dio estas Tiu, kiu elfaras en vi la volon kaj la energion laŭ Sia bonvolo.
\v 14 Ĉion faru sen murmuroj kaj disputoj,
\v 15 por ke vi estu senkulpaj kaj simplaj, filoj de Dio, neriproĉindaj, meze de perversa kaj malhonesta generacio, inter kiuj vi brilas kiel lumiloj en la mondo,
\v 16 forte tenante la vorton de la vivo; por ke estu por mi io, pri kio mi povos min gratuli en la tago de Kristo, ke mi ne vane kuris kaj laboris.
\v 17 Cetere, eĉ se mi estas elverŝata sur la oferon kaj servon de via fido,
\q mi ĝojas kaj kunĝojas kun vi ĉiuj;
\v 18 kaj tiel same vi ankaŭ ĝoju kaj kunĝoju kun mi.
\v 19 Sed mi esperas en la Sinjoro Jesuo baldaŭ sendi al vi Timoteon, por ke mi ankaŭ refreŝiĝu, kiam mi certiĝos pri via stato.
\v 20 Ĉar mi havas neniun samsentan, kiu sincere zorgos pri via stato.
\v 21 Ĉar ĉiu celas siajn aferojn, ne la aferojn de Jesuo Kristo.
\v 22 Sed vi ja konas lian provadon, ke kiel filo servas al sia patro, tiel li servis kun mi por la evangelio.
\v 23 Lin do mi esperas tuj sendi, kiam mi certiĝos, kio al mi okazos,
\v 24 sed mi fidas al la Sinjoro, ke mi mem ankaŭ venos baldaŭ.
\v 25 Tamen ŝajnis al mi necese sendi al vi Epafroditon, mian fraton kaj kunlaboranton kaj kunbatalanton, vian senditon kaj helpanton por mia bezono;
\v 26 ĉar li sopiris al vi ĉiuj, kaj forte maltrankviliĝis pro tio, ke vi jam sciiĝis pri lia malsano;
\v 27 ĉar efektive li malsanis, preskaŭ ĝis morto; sed Dio kompatis lin,
\q kaj ne sole lin, sed ankaŭ min, por ke mi ne havu malĝojon super malĝojo.
\v 28 Mi do sendis lin des pli diligente, por ke, revidante lin, vi ĝoju, kaj ke mi estu sen malĝojo.
\v 29 Akceptu lin do en la Sinjoro kun plena ĝojo, kaj tiajn homojn honoru;
\v 30 ĉar pro la laboro por Kristo li alproksimiĝis al morto, riskante sian vivon, por kompletigi tion, kio mankis ĉe via servado al mi.
\c 3
\p
\v 1 Fine, fratoj, ĝoju en la Sinjoro. Tute ne ĝenas min al vi skribi ripete, kaj por vi tio estas sendanĝeriga.
\v 2 Gardu vin kontraŭ la hundoj, gardu vin kontraŭ la malbonaguloj, gardu vin kontraŭ la karntranĉuloj;
\v 3 ĉar ni estas la cirkumciduloj, kiuj per la Spirito de Dio adoras, kaj nin gratulas pri Kristo Jesuo, kaj ne havas fidon al la karno;
\v 4 kvankam mi mem povas havi fidon al la karno; se iu alia pretendas fidi la karnon, mi ja pli efektive;
\v 5 cirkumcidita la okan tagon, el la raso Izraela, el la tribo de Benjamen,
\q Hebreo el Hebreoj; rilate la leĝon, Fariseo;
\v 6 rilate fervoron, persekutanto de la eklezio; rilate la justecon, kiu fariĝas per la leĝo, senkulpa.
\v 7 Sed kio estis al mi gajno, tion mi rigardis kiel malgajnon pro Kristo.
\v 8 Kaj efektive mi rigardas ĉion kiel malgajnon pro la supereco de la scio de Kristo Jesuo, mia Sinjoro, pro kiu mi suferis malgajnon de ĉio, kaj rigardas ĉion kiel rubon, por ke mi gajnu Kriston,
\v 9 kaj estu trovata en li, ne havante mian justecon, kiu devenas de la leĝo, sed la justecon, kiu estas per la fido al Kristo, la justecon, kiu devenas de Dio sur fidon;
\v 10 por ke mi sciu lin kaj la potencon de lia releviĝo kaj la kunulecon en liaj suferoj, konformiĝinte al lia morto,
\v 11 se mi povos iel atingi la releviĝon el la mortintoj.
\v 12 Ne kvazaŭ mi jam trafis, aŭ jam perfektiĝis; sed mi min pelas antaŭen, se mi nur povos ekteni tion, pro kio mi estas tenata de Kristo Jesuo.
\v 13 Miaj fratoj, mi ne rigardas min kiel jam ektenantan; sed nur jene: forgesante tion, kio estas malantaŭe, kaj strebante al tio, kio estas antaŭe,
\v 14 mi min pelas al la celo, por akiri la premion de la supera voko de Dio en Kristo Jesuo.
\v 15 Ĉiuj el ni do, kiuj estas perfektaj, tiel pensu; kaj se pri io vi pensas alie, tion ankaŭ Dio malkaŝos al vi;
\v 16 sed ĉiaokaze, laŭ tio, kiom ni jam atingis, ni iradu.
\v 17 Fratoj, estu kunimitantoj de mi; kaj observu tiujn, kiuj tiel iradas,
\q kiel vi havas nin kiel ekzemplon.
\v 18 Ĉar iradas multaj, pri kiuj mi ofte diris al vi, kaj nun eĉ plorante diras, ke ili estas malamikoj de la kruco de Kristo;
\v 19 ilia fino estas pereo, ilia dio estas la ventro, kaj ilia gloro estas en ilia honto, ili atentas surteraĵojn.
\v 20 Ĉar nia burĝrajto estas en la ĉielo, de kie ankaŭ ni atendas Savonton, la Sinjoron Jesuo Kristo,
\v 21 kiu denove elformos la korpon de nia humiliĝo, por ke ĝi konformiĝu al la korpo de lia gloro, laŭ la energio, per kiu li eĉ povas ĉion al si submeti.
\c 4
\p
\v 1 Tial, miaj fratoj amataj kaj alsopirataj, mia ĝojo kaj mia krono, tiel staru fortike en la Sinjoro, miaj amataj.
\v 2 Mi admonas Eŭodian, kaj mi admonas Sintiĥen, ke ili estu unuanimaj en la Sinjoro.
\v 3 Kaj mi petegas vin ankaŭ, sincera kunjugulo, helpu tiujn virinojn, ĉar ili kunlaboris kun mi en la evangelio, kune kun Klemento kaj miaj ceteraj kunlaborantoj, kies nomoj estas en la libro de vivo.
\v 4 Ĝoju en la Sinjoro ĉiam; denove mi diros: Ĝoju.
\v 5 Via mildeco estu konata al ĉiuj. La Sinjoro estas proksima.
\v 6 Pri nenio trozorgu; sed pri ĉio, per preĝo kaj peto kun dankesprimo,
\q viaj deziroj sciiĝu al Dio.
\v 7 Kaj la paco de Dio, kiu superas ĉian intelekton, gardos viajn korojn kaj pensojn en Kristo Jesuo.
\v 8 Fine, fratoj, kio ajn estas vera, kio ajn honesta, kio ajn justa, kio ajn ĉasta, kio ajn ŝatinda, kio ajn bonfama; se estas ia virto, se estas ia laŭdo, tion pripensu.
\v 9 Kion vi lernis kaj ricevis kaj aŭdis kaj vidis en mi, tion faru; kaj la Dio de paco estos kun vi.
\v 10 Sed mi forte ĝojas en la Sinjoro, ke vi jam nun revivigis vian zorgon pri mi; vi ja estis zorgemaj, sed mankis al vi oportuna tempo.
\v 11 Tamen mi ne parolas rilate bezonon; ĉar mi lernis, en kia ajn stato mi estas, en tio esti kontenta.
\v 12 Mi scias humiliĝi, kaj mi scias ankaŭ esti en abundeco; ĉie kaj ĉiel mi lernis la sekreton pleniĝi kaj malsati, havi abundon kaj havi mankon.
\v 13 Mi ĉion povas fari en tiu, kiu min fortikigas.
\v 14 Tamen vi bone agis, partoprenante kun mi en mia suferado.
\v 15 Kaj vi mem, Filipianoj, ankaŭ scias, ke en la komenco de la evangelio,
\q kiam mi foriris el Makedonujo, neniu eklezio komunikiĝis kun mi rilate donadon kaj ricevadon, krom vi solaj;
\v 16 ĉar eĉ en Tesaloniko vi pli ol unufoje sendis ion por mia bezono.
\v 17 Mi ne deziras la donacon; sed mi deziras la frukton, kiu plimultiĝos por via profito.
\v 18 Sed mi havas ĉion, kaj estas en abundeco; mi jam pleniĝis, ricevinte per Epafrodito vian senditaĵon, agrablan odoraĵon, oferon akceptindan, kiu plaĉas al Dio.
\v 19 Kaj mia Dio satigos ĉian vian bezonon, laŭ Sia riĉo en gloro en Kristo Jesuo.
\v 20 Nun al nia Dio kaj Patro estu gloro por ĉiam kaj eterne. Amen.
\v 21 Salutu ĉiun sanktulon en Kristo Jesuo. La fratoj, kiuj estas kun mi,
\q vin salutas.
\v 22 Ĉiuj sanktuloj vin salutas, precipe tiuj, kiuj estas el la domanaro de Cezaro.
\v 23 La graco de la Sinjoro Jesuo Kristo estu kun via spirito.
