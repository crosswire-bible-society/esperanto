\id 1PE
\h
\toc1
\toc2
\toc3
\c 1
\p
\v 1 Petro, apostolo de Jesuo Kristo, al la elektitoj, kiuj estas el la dispelitaro, pasloĝantaj en Ponto, Galatujo, Kapadokio, Azio, kaj Bitinio,
\v 2 laŭ la antaŭscio de Dio, la Patro, en sanktigo de la Spirito, por obeo kaj aspergo de la sango de Jesuo Kristo: Graco al vi kaj paco pligrandiĝu.
\v 3 Benata estu la Dio kaj Patro de nia Sinjoro Jesuo Kristo, kiu laŭ Sia granda kompato nin renaskis en esperon vivan per la releviĝo de Jesuo Kristo el la mortintoj,
\v 4 en heredaĵon ne pereontan, ne makulotan, ne velkontan, rezervitan en la ĉielo por vi,
\v 5 kiuj estas gardataj de la potenco de Dio per fido al savo preta malkaŝiĝi en la lasta tempo.
\v 6 En tio vi ĝojegas, kvankam por kelka tempo, se estas necese,
\q malĝojigite en diversaj tentoj,
\v 7 por ke la provado de via fido, pli multevalora ol oro, kiu pereas,
\q kvankam per fajro provite, troviĝu efika por laŭdo kaj gloro kaj honoro en la malkaŝo de Jesuo Kristo;
\v 8 kiun, ne vidinte, vi amas; kiun kvankam nun vi ne vidas, tamen, al li kredante, vi ĝojegas per ĝojo nedirebla kaj gloroplena;
\v 9 ricevante la celon de via fido, savon de animoj.
\v 10 Koncerne tiun savon esploris kaj serĉis la profetoj, kiuj profetis pri la graco venonta al vi;
\v 11 serĉante, kiun kaj kian tempon montris la enestanta en ili Spirito de Kristo, kiu atestis antaŭe la suferojn por Kristo kaj la sekvontajn glorojn.
\v 12 Kaj al ili malkaŝiĝis, ke ne al si mem, sed al vi, ili liveris tion,
\q kio nun estas proklamita al vi de tiuj, kiuj predikis al vi la evangelion per la Spirito Sankta, elsendita el la ĉielo; en kiujn aferojn anĝeloj deziras enrigardi.
\v 13 Tial, ĉirkaŭzoninte la lumbojn de via menso, estu sobraj kaj esperadu perfekte la gracon alportotan al vi en la malkaŝo de Jesuo Kristo;
\v 14 kiel infanoj de obeo, ne formiĝante laŭ la voluptoj, kiujn vi antaŭe havis en via nescio;
\v 15 sed kiel via Vokinto estas sankta, tiel vi ankaŭ fariĝu sanktaj en ĉia konduto;
\v 16 pro tio, ke estas skribite: Vi estu sanktaj, ĉar Mi estas sankta.
\v 17 Kaj se vi vokas la Patron, kiu sen personfavorado juĝas laŭ la faro de ĉiu, vi pasigu en timo la tempon de via ĉi tiea loĝado;
\v 18 sciante, ke ne per pereemaj objektoj, oro aŭ arĝento, vi elaĉetiĝis el via vanta vivmaniero, kiun vi ricevis de viaj patroj;
\v 19 sed per multekosta sango, kiel de ŝafido senmakula kaj sendifekta, la sango de Kristo;
\v 20 kiu estis antaŭdifinita antaŭ la fondo de la mondo, sed elmontrita en la fino de la tempoj pro vi,
\v 21 kiuj per li fidas Dion, kiu relevis lin el la mortintoj kaj donis al li gloron; por ke via fido kaj espero estu al Dio.
\v 22 Ĉastiginte viajn animojn, en la obeo al la vero, por sincera amo al la frataro, amu unu la alian el la koro fervore;
\v 23 renaskite, ne el pereema semo, sed el nepereema, per la vorto de Dio,
\q vivanta kaj restanta.
\v 24 Ĉar:
\q  Ĉiu karno estas herbo,
\q  Kaj ĉiu ĝia ĉarmo estas kiel kampa floreto.
\q  Sekiĝas herbo, velkas floreto;
\v 25 Sed la vorto de la Eternulo restas eterne.
\q Kaj ĉi tiu estas la parolo, kiu estas predikita al vi.
\c 2
\p
\v 1 Formetinte do ĉian malbonon kaj ĉian trompon kaj hipokritecon kaj enviojn kaj ĉiajn kalumniojn,
\v 2 kiel ĵusnaskitaj suĉinfanoj, sopiru al la spirita lakto pura, por ke vi per tio kresku en savon,
\v 3 se vi gustumis, ke la Sinjoro estas bona;
\v 4 al kiu alvenante, kvazaŭ al ŝtono vivanta, de homoj rifuzita, sed ĉe Dio elektita, honorinda,
\v 5 vi ankaŭ, kiel ŝtonoj vivantaj, konstruiĝas domo spirita, sankta pastraro, por oferi spiritajn oferojn, akcepteblajn de Dio per Jesuo Kristo.
\v 6 Ĉar estas enhavate en skribo:
\q 
\q 
\q Jen Mi kuŝigas en Cion bazangulan ŝtonon, elektitan, valoregan;
\q  Kaj kiu lin fidas, tiu ne estos hontigita.
\v 7 Por vi do, la fidantoj, estas la valoregeco; sed por nefidantoj:
\q 
\q 
\q Ŝtono, kiun malŝatis la konstruantoj,
\q  Fariĝis ŝtono bazangula;
\v 8 kaj:
\q 
\q 
\q Ŝtono de falpuŝiĝo kaj roko de alfrapiĝo;
\q ili falpuŝiĝas pro la vorto, malobeante, por kio ankaŭ ili estas difinitaj.
\v 9 Sed vi estas raso elektita, pastraro reĝa, nacio sankta, popolo Diposedata, por ke vi proklamu la gloron de Tiu, kiu vokis vin el mallumo en Sian lumon mirindan;
\v 10 kiuj iam estis ne-popolo, sed nun estas popolo de Dio; antaŭe nekompatitoj, sed nun kompatitoj.
\v 11 Amataj, mi petegas vin kiel fremdulojn kaj migrantojn, detenu vin de karnaj voluptoj, kiuj militadas kontraŭ la animo;
\v 12 kondutante dece ĉe la nacianoj, por ke, kvankam ili kalumnias vin kiel malbonagulojn, tamen, vidante viajn bonfarojn, ili gloru Dion en la tago de vizitado.
\v 13 Submetu vin al ĉiu homa institucio pro la Sinjoro; ĉu al la reĝo, la superreganto;
\v 14 ĉu al regantoj, kiel liaj senditoj por venĝo al malbonaguloj kaj por laŭdo al bonaguloj.
\v 15 Ĉar la volo de Dio estas, ke bonfarante, vi silentigu la nesciadon de homoj malsaĝaj;
\v 16 kiel liberaj, kaj ne farantaj el via libereco kovrilon de malico, sed kiel servistoj de Dio.
\v 17 Ĉiujn honoru. La frataron amu. Dion timu. La reĝon honoru.
\v 18 Servantoj, submetu vin al viaj sinjoroj kun ĉia timo; ne sole al la bonaj kaj malseveraj, sed ankaŭ al la malbonhumoraj.
\v 19 Ĉar tio estas laŭdinda, se pro konscienco al Dio oni elportas malĝojon, suferante maljuste.
\v 20 Ĉar kia honoro estas, se, pekante kaj sekve batate, vi tion pacience elportas? sed se, bonfarante kaj sekve suferante, vi tion pacience elportas,
\q tio estas laŭdinda antaŭ Dio.
\v 21 Ĉar al ĉi tio vi estas vokitaj; pro tio, ke Kristo ankaŭ suferis pro vi, postlasante por vi ekzemplon, por ke vi sekvu liajn postesignojn;
\v 22 pekon li ne faris, kaj trompo ne troviĝis en lia buŝo;
\v 23 insultate, li ne insultis responde; suferante, li ne minacis, sed submetis sin al la juste juĝanta;
\v 24 li mem portis niajn pekojn en sia korpo sur la lignaĵo, por ke ni,
\q malvivigite koncerne pekojn, vivu por justeco; per lia vundo vi resaniĝis.
\v 25 Ĉar vi estis kiel ŝafoj erarvagantaj; sed nun vi returne venis al la Paŝtisto kaj Episkopo de viaj animoj.
\c 3
\p
\v 1 Tiel same, edzinoj, submetu vin al viaj propraj edzoj; por ke, eĉ se iuj ne obeas al la vorto, ili sen la vorto estu gajnitaj per la konduto de siaj edzinoj,
\v 2 rigardante vian konduton kun timo ĉastan.
\v 3 Via ornamo estu ne la ekstera ornamo de harplektado kaj orportado, aŭ la surmetado de vestoj;
\v 4 sed la kaŝita homo de la koro en la nedifektebla vesto de spirito milda kaj trankvila, kiu estas multevalora antaŭ Dio.
\v 5 Ĉar tiel same ankaŭ, en la tempo antikva, la sanktaj virinoj, kiuj esperis al Dio, ornamis sin, submetante sin al siaj propraj edzoj;
\v 6 kiel Sara obeis al Abraham, nomante lin sinjoro; kies infanoj vi fariĝis, bonfarante, kaj ne ektimigate per ia teruro.
\v 7 Tiel same, edzoj, kunvivadu kun viaj edzinoj laŭscie, donante honoron al la virino kiel al la plimalforta ilo, kaj estante ankaŭ kun ili kunheredantoj de la graco de vivo, por ke viaj preĝoj ne malhelpiĝu.
\v 8 Fine, estu ĉiuj samideaj, simpatiaj, fratamemaj, kompatemaj,
\q humilanimaj;
\v 9 ne repagantaj malbonon kontraŭ malbono, nek insulton kontraŭ insulto,
\q sed kontraŭe benadantaj; ĉar al tio vi estas vokitaj, por ke vi heredu benon.
\v 10 Ĉar:
\q  Kiu volas ami vivon kaj vidi bonajn tagojn,
\q  Tiu gardu sian langon kontraŭ malbono,
\q  Kaj sian buŝon kontraŭ mensoga parolo;
\v 11 Li dekliniĝu de malbono kaj faru bonon,
\q  Li serĉu pacon, kaj ĉasu ĝin.
\v 12 Ĉar la okuloj de la Eternulo estas turnitaj al la piuloj,
\q  Kaj Liaj oreloj al iliaj krioj;
\q  Sed la vizaĝo de la Eternulo estas kontraŭ tiuj, kiuj faras malbonon.
\v 13 Kaj kiu faros al vi malbonon, se vi fariĝos fervoraj en la bono?
\v 14 Sed se vi eĉ suferus pro justeco, feliĉegaj vi estus; kaj ilian timon ne timu, nek maltrankviliĝu;
\v 15 sed la Sinjoron Kriston sanktigu en viaj koroj; estu ĉiam pretaj doni defendan respondon al ĉiu, kiu vin demandas pri la motivo de la espero en vi, sed kun humileco kaj timo;
\v 16 havantaj bonan konsciencon; por ke, dum vi estas malestimataj, hontu tiuj, kiuj kalumnias vian bonan konduton en Kristo.
\v 17 Ĉar pli bone estus, se tiel volus Dio, suferi pro bonfarado, ol pro malbonfarado.
\v 18 Ĉar Kristo ankaŭ unufoje suferis pro pekoj, justulo pro maljustuloj,
\q por ke li nin konduku al Dio; mortigite en la karno, sed vivigite en la spirito;
\v 19 en kiu ankaŭ li iris kaj predikis al la enkarceraj spiritoj,
\v 20 kiuj iam malobeis, kiam la longedaŭra pacienco de Dio atendis en la tagoj de Noa dum la pretigado de la arkeo, en kiu malmultaj, tio estas ok personoj, elsaviĝis tra akvo;
\v 21 kiu ankaŭ vin nun savas en antitipo, la bapto, ne la formetado de la karna malpuraĵo, sed la demando de bona konscienco al Dio per la releviĝo de Jesuo Kristo;
\v 22 kiu estas dekstre de Dio, irinte en la ĉielon; al li anĝeloj kaj aŭtoritatoj kaj potencoj estas submetitaj.
\c 4
\p
\v 1 Ĉar Kristo do suferis en la karno, vi ankaŭ armu vin per la sama intenco; ĉar la suferinto en la karno apartiĝis de pekoj;
\v 2 por ke vi travivu la reston de la enkarna tempo jam ne laŭ la voluptoj de homoj, sed laŭ la volo de Dio.
\v 3 Ĉar la tempo pasinta sufiĉas, por elfari la deziron de la nacianoj kaj iradi en diboĉoj, voluptoj, vindrinkado, brufestenoj, ebrieco, kaj abomenaj idolkultoj;
\v 4 en kio ili miras, ke vi ne kuras kune kun ili en la saman superfluon de diboĉado, kaj ili kalumnias vin;
\v 5 ili prirespondos al Tiu, kiu estas preta juĝi la vivantojn kaj la mortintojn.
\v 6 Ĉar por tio ankaŭ la evangelio estas anoncita al la mortintoj, por ke ili estu korpe juĝitaj laŭ homoj, sed spirite vivu laŭ Dio.
\v 7 Sed la fino de ĉio alproksimiĝas; prudentiĝu do, kaj sobriĝu por preĝoj;
\v 8 antaŭ ĉio havante fervoran amon unu al alia; ĉar amo kovras amason da pekoj;
\v 9 estu gastamaj unu al alia sen murmurado;
\v 10 laŭmezure, kiel ĉiu ricevis donacon, tiel ĝin administrante inter vi,
\q kiel bonaj administrantoj de la diversaspeca graco de Dio;
\v 11 se iu parolas, li parolu kvazaŭ orakolojn de Dio; se iu administras, li administru kvazaŭ el la forto, kiun Dio provizas; por ke en ĉio Dio estu glorata per Jesuo Kristo, kies estas la gloro kaj la potenco por ĉiam kaj eterne. Amen.
\v 12 Amataj, ne surpriziĝu pri la fajrego ĉe vi okazanta por provi vin,
\q kvazaŭ io stranga okazus ĉe vi;
\v 13 sed laŭ tio, ke vi partoprenas en la suferoj de Kristo, ĝoju; por ke ankaŭ en la elmontro de lia gloro vi ĝoju ravege.
\v 14 Se vi estas riproĉataj pro la nomo de Kristo, feliĉegaj vi estas; ĉar la Spirito de gloro kaj la Spirito de Dio restas sur vi.
\v 15 Nur neniu el vi suferu kiel mortiginto, aŭ ŝtelisto, aŭ malbonfarinto, aŭ kiel sintrudanto en malpropraj aferoj;
\v 16 sed se iu suferas kiel Kristano, li ne hontu; sed li gloru Dion en tiu nomo.
\v 17 Ĉar jam venis la tempo por la komenco de la juĝo ĉe la domo de Dio;
\q kaj se ĝi komenciĝas ĉe ni, kia estos la sorto de tiuj, kiuj ne obeas al la evangelio de Dio?
\v 18 Kaj se justulo apenaŭ saviĝas, kie aperos malpiulo kaj pekulo?
\v 19 Tial ankaŭ tiuj, kiuj suferas laŭ la volo de Dio, alkonfidu siajn animojn en bonfarado al fidela Kreinto.
\c 5
\p
\v 1 La presbiterojn do inter vi mi admonas, estante kunpresbitero kaj atestanto de la suferoj de Kristo kaj partoprenanto en la malkaŝota gloro:
\v 2 Paŝtu la gregon de Dio ĉe vi, direktante ĝin, ne devige, sed volonte,
\q laŭ Dio; ne pro avideco, sed bonvole;
\v 3 ne kvazaŭ sinjorante super la heredo, sed fariĝante ekzemploj al la grego.
\v 4 Kaj kiam elmontriĝos la Ĉefpaŝtisto, vi ricevos la nevelkontan kronon de gloro.
\v 5 Tiel same, vi plijunuloj, submetu vin al la pliaĝuloj. Tial ankaŭ vi ĉiuj submetu vin unu al la alia, kaj zonu vin per humileco; ĉar Dio kontraŭstaras al la fieruloj, sed al la humiluloj donas gracon.
\v 6 Humiligu vin do sub la potencan manon de Dio, por ke Li altigu vin ĝustatempe;
\v 7 surĵetante sur Lin ĉian vian zorgon, ĉar Li zorgas pri vi.
\v 8 Estu sobraj, vigladu; via kontraŭulo, la diablo, kiel leono blekeganta ĉirkaŭiras, serĉante, kiun li povos forgluti;
\v 9 lin rezistu, konstantaj en la fido, sciante, ke la samaj suferoj plenumiĝas en via frataro en la mondo.
\v 10 Kaj la Dio de ĉia graco, kiu vin alvokis al Sia eterna gloro en Kristo,
\q mem perfektigos, firmigos, plifortigos vin ne longe suferintajn.
\v 11 Al Li estu la potenco por ĉiam kaj eterne. Amen.
\v 12 Per Silvano, la fidela frato, kiel mi opinias, al vi mi skribis mallonge, konsilante kaj atestante, ke ĉi tio estas la vera graco de Dio; en ĝi firme staru.
\v 13 Vin salutas la en Babel kunelektitino, kaj Marko, mia filo.
\v 14 Salutu unu la alian per ama kiso.
\q Paco al vi ĉiuj, kiuj estas en Kristo.
