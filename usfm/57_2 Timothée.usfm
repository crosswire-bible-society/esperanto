\id 2TI
\h
\toc1
\toc2
\toc3
\c 1
\p
\v 1 Paŭlo, apostolo de Kristo Jesuo, per la volo de Dio, laŭ la promeso de vivo, kiu estas en Kristo Jesuo,
\v 2 al Timoteo, mia amata filo: Graco, kompato, kaj paco de Dio, la Patro, kaj de Kristo Jesuo, nia Sinjoro.
\v 3 Mi dankas Dion, kiun mi adoras de post miaj praavoj kun pura konscienco,
\q ke konstantan memoron mi havas pri vi en miaj preĝoj, nokte kaj tage
\v 4 sopirante vidi vin, memorante viajn larmojn, por ke mi pleniĝu de ĝojo;
\v 5 ricevinte rememoron pri la sincera fido, kiu estas en vi, kaj kiu loĝis unue en via avino Lois kaj en via patrino Eŭnike, kaj, mi konvinkiĝis, en vi ankaŭ.
\v 6 Pro tio mi memorigas vin, ke vi reekbruligu la donacon de Dio, kiu estas en vi per la surmetado de miaj manoj.
\v 7 Ĉar Dio donis al ni spiriton ne de malkuraĝeco, sed de potenco kaj amo kaj sinregado.
\v 8 Ne hontu do pri la atesto de nia Sinjoro, nek pri mi, lia malliberulo;
\q sed kunelportu suferojn kun la evangelio laŭ la potenco de Dio,
\v 9 kiu nin savis kaj vokis per sankta voko, ne laŭ niaj faroj, sed laŭ Sia propra antaŭintenco kaj laŭ la graco donita al ni en Kristo Jesuo antaŭ tempoj eternaj,
\v 10 sed nun malkaŝita per la apero de nia Savanto Jesuo Kristo, kiu neniigis la morton kaj enlumigis la vivon kaj la senmortecon per la evangelio,
\v 11 al kiu mi estas nomita predikisto kaj apostolo kaj instruisto.
\v 12 Pro tio mi ankaŭ suferas tiel; tamen mi ne hontas; ĉar mi konas tiun,
\q al kiu mi kredis, kaj mi konvinkiĝis, ke li havas la povon gardi mian konfiditaĵon ĝis tiu tago.
\v 13 Konservu la modelon de sanaj vortoj, kiujn vi aŭdis de mi, en fido kaj amo, kiuj estas en Kristo Jesuo.
\v 14 La bonan konfiditaĵon gardu per la Sankta Spirito, kiu loĝas en ni.
\v 15 Vi scias, ke forturniĝis de mi ĉiuj en Azio, el kiuj estas Figelo kaj Hermogenes.
\v 16 La Sinjoro donu kompaton al la domo de Onesiforo; ĉar li ofte min refreŝigis, kaj li ne hontis pri mia kateno,
\v 17 sed kiam li estis en Romo, li elserĉis min tre diligente kaj min trovis
\v 18 (la Sinjoro donu al li, ke li trovu kompaton ĉe la Sinjoro en tiu tago), kaj vi scias tre bone, kiom da servoj li faris en Efeso.
\c 2
\p
\v 1 Vi do, mia filo, fortikiĝu en la graco, kiu estas en Kristo Jesuo.
\v 2 Kaj kion vi de mi aŭdis inter multaj atestantoj, tion transdonu al fidelaj homoj, kompetentaj instrui ankaŭ aliajn.
\v 3 Vi do elportu suferojn, kiel bona militisto de Jesuo Kristo.
\v 4 Neniu militanto sin implikas en la aferojn de ĉi tiu vivo; por ke li plaĉu al sia varbinto.
\v 5 Kaj se iu konkuras kiel atleto, li ne estas kronata, se li ne laŭregule konkuris.
\v 6 La laboranta terkultivisto devas la unua partopreni en la fruktoj.
\v 7 Konsideru tion, kion mi diras; ĉar la Sinjoro donos al vi komprenon pri ĉio.
\v 8 Memoru Jesuon Kriston, levitan el la mortintoj, el la idaro de David,
\q laŭ mia evangelio;
\v 9 en ĝi mi elportas suferadon ĝis ligiloj, kiel krimulo, sed la vorto de Dio ne estas ligita.
\v 10 Tial mi suferas ĉion pro la elektitoj, por ke ili ankaŭ atingu la en Kristo Jesuo savon kun gloro eterna.
\v 11 Fidinda estas la diro: Ĉar se ni mortis kun li, ni ankaŭ vivos kun li;
\v 12 se ni suferas, ni ankaŭ reĝos kun li; se ni malkonfesos lin, li ankaŭ nin malkonfesos;
\v 13 se ni malfidas, li restas fidela; ĉar li ne povas malkonfesi sin mem.
\v 14 Memorigu ilin pri tio, admonante ilin antaŭ la Sinjoro, ke ili ne vortobatalu, kio estas neniel utila, kun risko de ŝancelo por la aŭdantoj.
\v 15 Klopodu prezenti vin ĉe Dio kiel aprobita, laboranto senriproĉa,
\q ĝuste pritaksante la vorton de la vero.
\v 16 Sed evitu profanajn babiladojn; ĉar ili iros antaŭen al pli multe da malpieco,
\v 17 kaj ilia parolo dismordos kiel gangreno; el kiuj estas Himeneo kaj Fileto,
\v 18 kiuj pri la vero eraris, dirante, ke la releviĝo estas jam pasinta; kaj ili renversas la fidon de kelkaj.
\v 19 Tamen la firma fundamento de Dio staras, havante la jenan sigelon: La Sinjoro konas tiujn, kiuj estas liaj, kaj: Foriru de maljusteco ĉiu, kiu nomas la nomon de la Sinjoro.
\v 20 Sed en granda domo estas vazoj ne nur oraj kaj arĝentaj, sed ankaŭ lignaj kaj argilaj, kaj unuj por honoro kaj aliaj por malhonoro.
\v 21 Se do iu purigos sin de ĉi tiuj, li estos vazo por honoro, sanktigita,
\q taŭga por la mastro, pretigita por ĉiu bona laboro.
\v 22 Sed forkuru de junulaj voluptoj, kaj sekvu justecon, fidon, amon, pacon,
\q kune kun tiuj, kiuj vokas la Sinjoron el pura koro.
\v 23 Sed malsaĝajn kaj neklerajn demandojn evitu, sciante, ke ili naskas malpacojn.
\v 24 Kaj la servisto de la Sinjoro devas ne malpaci, sed esti afabla al ĉiuj, instruema, tolerema,
\v 25 en humileco instruante tiujn, kiuj kontraŭstaras; eble Dio donos al ili penton, por ke ili venu al scio de la vero,
\v 26 kaj por ke ili sobraj eliĝu el la kaptilo de la diablo, kaptite de li,
\q por plenumi lian volon.
\c 3
\p
\v 1 Sed sciu, ke en la lastaj tagoj venos danĝeraj tempoj.
\v 2 Ĉar homoj estos sinamantaj, monamantaj, fanfaronemaj, arogantaj,
\q insultemaj, malobeemaj al gepatroj, sendankaj, nesanktaj,
\v 3 neparencamaj, nepacigeblaj, kalumniemaj, nesinregantaj, malmildaj, ne bonamantaj,
\v 4 perfidemaj, pasiemaj, ventoplenaj, plezuron amantaj pli ol Dion;
\v 5 havante ŝajnon de pieco, sed neinte ĝian potencon; de ĉi tiuj ankaŭ vin forturnu.
\v 6 Ĉar el ĉi tiuj estas tiuj, kiuj rampas en domojn, kaj forkaptas malsaĝajn virinojn, ŝarĝitajn de pekoj, forkondukatajn per diversaj voluptoj,
\v 7 ĉiam lernantajn, kaj neniam kapablajn veni al la scio de la vero.
\v 8 Kaj kiel Janes kaj Jambres kontraŭstaris al Moseo, tiel same ĉi tiuj ankaŭ kontraŭstaras al la vero, homoj kun malnobligita spirito, por la fido senvaloraj.
\v 9 Sed ili ne plu iros antaŭen; ĉar ilia malsaĝeco evidentiĝos al ĉiuj,
\q kiel ankaŭ fariĝis ĉe tiuj.
\v 10 Sed vi sekvis mian instruon, konduton, celon, fidon, toleremecon, amon,
\q paciencon,
\v 11 persekutojn, suferojn, kiaj okazis al mi en Antioĥia, en Ikonio, en Listra; kiajn persekutojn mi suferis; kaj el ĉio tio la Sinjoro min savis.
\v 12 Jes, kaj ĉiuj, kiuj volas vivi pie en Kristo Jesuo, estos persekutataj.
\v 13 Sed malbonaj homoj kaj ruzuloj ĉiam iros antaŭen al pli granda malbono, trompantaj kaj trompataj.
\v 14 Sed restu vi en tio, kion vi lernis kaj pri kio vi certiĝis, sciante,
\q de kiu vi ĝin lernis,
\v 15 kaj ke jam de frua infaneco vi konas la sanktajn skribojn, kiuj povas fari vin saĝa por savo per la fido, kiu estas en Kristo Jesuo.
\v 16 Ĉiu skribaĵo inspirita de Dio estas ankaŭ utila por instruo, por admono, por korekto, por disciplino en justeco;
\v 17 por ke la homo de Dio estu perfekta, plene provizita por ĉiu bona laboro.
\c 4
\p
\v 1 Mi ordonas al vi antaŭ Dio, kaj antaŭ Kristo Jesuo, kiu juĝos la vivantojn kaj la mortintojn, kaj pro lia apero kaj lia regno;
\v 2 prediku la vorton; insistu ĝustatempe, malĝustatempe; konvinku, admonu,
\q konsilu, en ĉia pacienco kaj instruado.
\v 3 Ĉar venos tempo, kiam oni ne toleros la sanan instruon; sed havante jukantajn orelojn, amasigos al si instruantojn, laŭ siaj deziroj;
\v 4 kaj ili deturnos de la vero sian orelon, kaj turnos sin flanken al fabeloj.
\v 5 Sed vi estu sobra en ĉio, elportu suferojn, faru la laboron de evangeliisto, plenumu vian servadon.
\v 6 Ĉar mi jam estas elverŝata kvazaŭ verŝofero, kaj venis la horo de mia foriro.
\v 7 Mi batalis la bonan batalon, mi finis la kuradon, mi gardis la fidon;
\v 8 de nun estas konservita por mi la krono de justeco, kiun la Sinjoro, la justa juĝisto, donos al mi en tiu tago; kaj ne nur al mi, sed ankaŭ al ĉiuj, kiuj amis lian aperon.
\v 9 Klopodu veni baldaŭ al mi;
\v 10 ĉar Demas min forlasis, amante la nunan mondon, kaj iris al Tesaloniko;
\q Kreskens al Galatujo, Tito al Dalmatujo.
\v 11 Nur Luko estas kun mi. Prenu Markon kaj alkonduku lin kun vi, ĉar li utilas al mi por la servado.
\v 12 Sed Tiĥikon mi sendis al Efeso.
\v 13 La mantelon, kiun mi lasis en Troas ĉe Karpo, alportu, kiam vi venos,
\q kaj la librojn, precipe la pergamenojn.
\v 14 Aleksandro, la kupristo, faris al mi multe da malbono; la Sinjoro redonos al li laŭ liaj faroj;
\v 15 kontraŭ li vi ankaŭ vin gardu, ĉar li multege kontraŭstaris al niaj vortoj.
\v 16 Ĉe mia unua pledo, neniu staris kun mi, sed ĉiuj min forlasis; tio ne estu kalkulita kontraŭ ili.
\v 17 Sed la Sinjoro staris apud mi, kaj min fortigis, por ke per mi la anonco estu plene publikigita, kaj ke ĉiuj nacianoj aŭdu; kaj mi forsaviĝis el la buŝo de la leono.
\v 18 La Sinjoro min savos de ĉiu malbona faro, kaj min savkondukos en sian ĉielan regnon; al li la gloro por ĉiam kaj eterne. Amen.
\v 19 Salutu Priskilan kaj Akvilan kaj la domon de Onesiforo.
\v 20 Erasto restis en Korinto; sed Trofimon mi lasis malsanan en Mileto.
\v 21 Klopodu veni antaŭ la vintro. Salutas vin Eŭbulo kaj Pudens kaj Lino kaj Klaŭdia kaj ĉiuj fratoj.
\v 22 La Sinjoro estu kun via spirito. Graco estu kun vi.
